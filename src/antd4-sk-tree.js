
import { AntdSkTree }  from '../../sk-tree-antd/src/antd-sk-tree.js';

export class Antd4SkTree extends AntdSkTree {

    get prefix() {
        return 'antd4';
    }

}
